Include "--recurse-submodules" argument when cloning this repository to resolve all of the git sub-module references

```bash
git clone --recurse-submodules https://penniman26@bitbucket.org/pennidinh/dockerbuilder-pennidinhhomehub.git
```
