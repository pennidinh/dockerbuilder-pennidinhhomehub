#!/usr/bin/ruby

require 'date'

CONFIG=[
  {:path => './pennidinhhomehub-apacheserver', :tag => 'penniman26/apacheserver:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-apacheserver.git'},
  {:path => './pennidinhhomehub-nodejsserver', :tag => 'penniman26/nodejsserver:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-nodejsserver.git'},
  {:path => './pennidinhhomehub-subdomainownershipdaemon', :tag => 'penniman26/subdomain-ownership-daemon:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-subdomainownershipdaemon.git'},
  {:path => './pennidinhhomehub-openvpn', :tag => 'penniman26/openvpn:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-openvpn.git'},
  {:path => './pennidinhhomehub-thumbnailprocessor', :tag => 'penniman26/thumbnail-processor:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-thumbnailprocessor.git'},
  {:path => './pennidinhhomehub-diskspacemaid', :tag => 'penniman26/diskspacemaid:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-diskspacemaid.git'},
  {:path => './pennidinhhomehub-proxydaemon', :tag => 'penniman26/proxy-daemon:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-proxydaemon.git'},
  {:path => './pennidinhhomehub-thumbnailsweeper', :tag => 'penniman26/thumbnail-sweeper:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-thumbnailsweeper.git'},
  {:path => './pennidinhhomehub-filelistener', :tag => 'penniman26/file-listener:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-filelistener.git'},
  {:path => './pennidinhhomehub-sshdaemon', :tag => 'penniman26/ssh-daemon:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-sshdaemon.git'},
  {:path => './pennidinhhomehub-ftpdaemon', :tag => 'penniman26/ftpdaemon:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhhomehub-ftpdaemon.git'},
  {:path => './pennidinhsharedproxy-apache', :tag => 'penniman26/shared-apache-proxy:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhsharedproxy-apache.git'},
  {:path => './pennidinhsharedproxy-sshusers', :tag => 'penniman26/ssh-proxy-server:x86-latest', :repo => 'git@bitbucket.org:pennidinh/pennidinhsharedproxy-sshusers.git'},
]

#validate
puts DateTime.now.strftime("%d/%m/%Y %H:%M")
puts 'Validating configs...'
for config in CONFIG do
   puts config[:path]
   puts config[:tag]
   puts config[:repo]
   if config[:path].nil?
     raise 'path cannot be nil!'
   end
   if config[:tag].nil?
     raise 'tag cannot be nil!'
   end
   if config[:repo].nil?
     raise 'repo cannot be nil!'
   end
end
puts 'Configs valid'
puts

puts 'Fetching all updates'
puts 'git submodule update --recursive --remote'
puts `git submodule update --recursive --remote`

for config in CONFIG do
  for command in ["docker build . -t #{config[:tag]}", "docker push #{config[:tag]}"] do
    puts DateTime.now.strftime("%d/%m/%Y %H:%M")
    puts "cd #{config[:path]} && #{command}"
    puts DateTime.now.strftime("%d/%m/%Y %H:%M")
    puts `cd #{config[:path]} && #{command}`
  end
end
puts DateTime.now.strftime("%d/%m/%Y %H:%M")
